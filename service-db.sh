
#!/bin/bash
set -eu

echo "User: $(whoami)"

HOST_USR="$(whoami)"

DB_VOLUME=AIRFLOW-DB-VOLUME
# DB_SOCKET_DIR=${HOME}/docker-sockets/mysql-airflow
DB_SOCKET_DIR=AIRFLOW-DB-SOCKET

DB_CONTAINER_NAME=MYSQL-AIRFLOW-META
DB_DOCKER_TAG=mysql5-airflow

#===================8<==========================================
# Handle args:
mode="undefined"
for ARGUMENT in "$@"
do
    case "$ARGUMENT" in
        start)      mode='run'; echo "Starting container..."; ;;
        restart)    mode='run'; echo "Restarting container..."; ;;     
        stop)       mode='stop'; echo "Stopping container..."; ;;     
        *)          echo "Usage: $0 (start|stop|restart)"; exit 1 ;;
    esac
done

if [ "do${mode}" == 'doundefined' ]; then echo "Invocation: $0 (start|stop)"; exit 1; fi
# Args processed

mount_dir() {
    # Create the directory if it doesn't exist
    DIR=$1
    test -d $DIR || mkdir -p $DIR && chmod 0777 $DIR && echo "$2"
}

#===================8<==========================================

docker stop "${DB_CONTAINER_NAME}"  &> /dev/null || true && docker rm --force "${DB_CONTAINER_NAME}"  &> /dev/null || true

docker volume rm $DB_SOCKET_DIR || echo "Socket not found";

if [ "do${mode}" == 'dostop' ]; then echo "${DB_CONTAINER_NAME} is not running now"; exit 0; fi

# mount_dir "${DB_SOCKET_DIR}" "DB soket dir - ready"

docker container inspect $DB_VOLUME || docker volume create $DB_VOLUME || true;

docker volume create $DB_SOCKET_DIR || echo "Cannot create socket" && true;


# ========================================

cd mysql

docker build --build-arg mysql_uid=$(id -u "${HOST_USR}") --build-arg mysql_gid=$(id -g "${HOST_USR}") . -t ${DB_DOCKER_TAG} 

docker run \
    -d \
    --restart=always \
    --name=${DB_CONTAINER_NAME} \
    --env="MYSQL_ROOT_PASSWORD=pass" \
    --env="MYSQL_DATABASE=airflow" \
    -v $(pwd)/initdb:/docker-entrypoint-initdb.d:ro \
    -v ${DB_VOLUME}:/var/lib/mysql \
    -v $(pwd)/cnf:/etc/mysql/conf.d \
    -v ${DB_SOCKET_DIR}:/var/run/mysqld \
    ${DB_DOCKER_TAG} \
    && echo "Starting MySQL container with volume: ${DB_VOLUME}" && cd ..

