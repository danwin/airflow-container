#!/bin/bash
set -eu

export AIRFLOW_CONN_MYSQL_MAIN="mysql://${DB_CONN_STR}?charset=utf8&cursor=dictcursor"

airflow initdb
# sleep 3 
# echo "********************************** Init DB - done [Ok]"
# airflow webserver
airflow webserver 2>&1 &
echo "********************************** Aiflow webserver is running now:)"
# airflow scheduler 2>&1 &
airflow scheduler
echo "********************************** Aiflow scheduler is running now:)"

# exec "$@"