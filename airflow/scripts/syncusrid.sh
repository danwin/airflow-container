#!/usr/bin/bash

set -eu
 
function main {
    local uid="${1:-}"
    local gid="${2:-}"

    # Change the uid
    if [[ -n "${uid:-}" ]]; then
        usermod -u "${uid}" airflow
    fi
    # Change the gid
    if [[ -n "${gid:-}" ]]; then
        groupmod -o -g "${gid}" airflow
    fi

    # # Setup permissions on the run directory where the sockets will be
    # # created, so we are sure the app will have the rights to create them.

    # # Make sure the folder exists.
    # mkdir -p "${path}"
    # # Set owner.
    # chown root:mysql "${path}"
    # # Set permissions.
    # chmod u=rwX,g=rwX,o=--- "${path}"
}

main "$@"