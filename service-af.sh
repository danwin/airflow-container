
#!/bin/bash
set -eu

HOST_USR="$(whoami)"

# DB_SOCKET_DIR=${HOME}/docker-sockets/mysql-airflow
DB_SOCKET_DIR=AIRFLOW-DB-SOCKET
BUFFER_VOLUME=AIRFLOW-DATA-BUFFER
# DAGS_PATH=~/my-dags-here
CONTAINER_NAME=AIRFLOW
DOCKER_TAG=airflow

#===================8<==========================================
# Handle args:
mode="undefined"
for ARGUMENT in "$@"
do
    case "$ARGUMENT" in
        start)      mode='run'; echo "Starting container..."; ;;
        restart)    mode='run'; echo "Restarting container..."; ;;     
        stop)       mode='stop'; echo "Stopping container..."; ;;     
        *)          echo "Usage: $0 (start|stop|restart)"; exit 1 ;;
    esac
done

if [ "do${mode}" == 'doundefined' ]; then echo "Invocation: $0 (start|stop)"; exit 1; fi
# Args processed

mount_dir() {
    # Create the directory if it doesn't exist
    DIR=$1
    test -d $DIR || mkdir -p $DIR && chmod 0775 $DIR && echo "$2"
}

#===================8<==========================================

docker stop "${CONTAINER_NAME}"  &> /dev/null || true && docker rm --force "${CONTAINER_NAME}"  &> /dev/null || true

if [ "do${mode}" == 'dostop' ]; then echo "${CONTAINER_NAME} is not running now"; exit 0; fi

# mount_dir "${DB_SOCKET_DIR}" "DB soket dir - ready"

docker container inspect ${BUFFER_VOLUME} || docker volume create ${BUFFER_VOLUME} || true;

# ========================================

cd airflow

docker build --build-arg cntusr_uid=$(id -u "${HOST_USR}") --build-arg cntusr_gid=$(id -g "${HOST_USR}") . -t ${DOCKER_TAG} 

docker run \
    -d \
    -p 8888:8081 \
    --restart=always \
    --name=${CONTAINER_NAME} \
    --env-file conf/env.list \
    -v ${DAGS_PATH}:/usr/local/airflow/dags \
    -v ${DB_SOCKET_DIR}:/var/run/db-socket \
    -v ${BUFFER_VOLUME}:/tmp/airflow \
    ${DOCKER_TAG} \
    && echo "Starting Airflow container" && cd ..

