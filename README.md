# Apache Airflow - Docker containers for deployment #
This repo contains 2 parts - Apache Airflow and MySQL. This MySQL instance is intended for Airflow internal needs (to use different "application" and "service" DBs and to decrease mutual dependencies). In contrast, the "application" DB should be used only for 
## How To Use ##
1. Create file env.list inside airflow/conf and define connection string for your "application" DB:
```
DB_CONN_STR=<user>:<password>@<host>:<port>
```
2. Run MySQL instance:
```
$ ./service-db.sh  start
```
3. Wait while DB runs initial scriupts at the firs time (you could look at logs, usign, for instance portainer.io ).

4. Run Airflow instance (don't forget to point your dags folder:):
```
$ DAGS_PATH=~/my-dags-here ./service-af.sh  start
```

You cold use some tools like a docker-compose to automate deployment. 
____

_No intended for production without your mind and care:)_